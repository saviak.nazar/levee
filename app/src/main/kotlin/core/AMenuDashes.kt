package core

import adblocker.DASH_ID_HOSTS_COUNT
import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.text.style.ImageSpan
import android.view.View
import android.widget.PopupMenu
import com.github.salomonbrys.kodein.instance
import core.ContentActor.Companion.X_END
import gs.environment.inject
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import org.blokada.R
import update.DASH_ID_ABOUT
import update.DASH_ID_OPTIONS
import core.DASH_ID_DNS
import filter.DASH_ID_BLACKLIST
import filter.DASH_ID_WHITELIST
import notification.DASH_ID_KEEPALIVE
import org.blokada.R.mipmap.ic_launcher
import android.text.SpannableString
import org.blokada.R.mipmap.ic_launcher
import android.text.style.ForegroundColorSpan
import android.support.v4.content.ContextCompat
import android.graphics.drawable.Drawable
import android.text.TextUtils


class DashNoop : Dash("main_noop", icon = false)
/*
class DashMainMenu(
        val ctx: Context,
        val ui: UiState,
        val contentActor: ContentActor
) : Dash(
        "main_menu",
        R.drawable.ic_more
) {

    private var menu: PopupMenu? = null

    init {
        onClick = { dashRef ->
            if (menu == null && dashRef is View) {
                menu = PopupMenu(ctx, dashRef)
                val m = menu!!.menu
                m.add(0, 1, 1, R.string.main_blog_text)
                m.add(0, 2, 2, R.string.main_cta)
                m.add(0, 3, 3, R.string.main_donate_text)
                m.add(0, 4, 4, R.string.main_faq_text)
                m.add(0, 5, 5, R.string.main_feedback_text)
                m.add(0, 6, 6, R.string.main_log)
                m.add(0, 7, 7, R.string.main_credits)
                m.add(0, 8, 8, R.string.update_about)
             //   m.add(0, 12, 12, R.string.dns_keepalive_content)


                menu!!.setOnMenuItemClickListener { openMenu(it.itemId); true }
            }
            menu!!.show()
            false
        }
    }

    private val openMenu = { itemId: Int ->
        val dash = when (itemId) {
            1 -> ui.dashes().firstOrNull { it.id == DASH_ID_BLOG }
            2 -> ui.dashes().firstOrNull { it.id == DASH_ID_CTA }
            3 -> ui.dashes().firstOrNull { it.id == DASH_ID_DONATE }
            4 -> ui.dashes().firstOrNull { it.id == DASH_ID_FAQ }
            5 -> ui.dashes().firstOrNull { it.id == DASH_ID_FEEDBACK }
            6 -> ui.dashes().firstOrNull { it.id == DASH_ID_LOG }
            7 -> ui.dashes().firstOrNull { it.id == DASH_ID_CREDITS }
            8 -> ui.dashes().firstOrNull { it.id == DASH_ID_ABOUT }
           // 12-> ui.dashes().firstOrNull { it.id == DASH_ID_KEEPALIVE }
            else -> null
        }
        when {
            dash == null -> Unit
            !dash.hasView -> dash.onClick?.invoke(0)
            else -> {
                contentActor.back {
                    contentActor.reveal(dash, X_END, 0)
                }
            }
        }
    }
}
*/


class TunnelDashHostsCount(
        val ctx: Context,
        val s: Filters = ctx.inject().instance(),
        val cmd: Commands = ctx.inject().instance()
) : Dash(DASH_ID_HOSTS_COUNT,
        R.drawable.ic_spam_filter,
        ctx.getBrandedString(R.string.tunnel_hosts_desc),
        onClick = { cmd.send(SyncFilters()); cmd.send(SyncHostsCache()); true }
) {

    init {
        launch {
            val request = MonitorHostsCount()
            cmd.subscribe(request).consumeEach {
                launch(UI) {
                    text = getCountString(it)}
            }
        }
    }

    private fun getCountString(count: Int): CharSequence {
         if (count >= 0){
            val wordtoSpan = SpannableString("b")
            val d = ContextCompat.getDrawable(ctx, R.drawable.ic_refresh)
            d!!.setBounds(0, 1, d.intrinsicWidth, d.intrinsicHeight)
            val span = ImageSpan(d, ImageSpan.ALIGN_BASELINE)
            wordtoSpan.setSpan(span, 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
            return TextUtils.concat(ctx.resources.getString(R.string.tunnel_hosts_count, count), "", wordtoSpan)

            }
        else
         {return ctx.resources.getString(R.string.tunnel_hosts_updating)}
    }
}

