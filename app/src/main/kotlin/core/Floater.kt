package core

import android.content.Context
import android.support.design.widget.FloatingActionButton
import android.support.v7.content.res.AppCompatResources.getColorStateList
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.TextView
import gs.presentation.doAfter
import gs.presentation.toPx
import org.blokada.R

class AFloaterView(
        ctx: Context,
        attributeSet: AttributeSet
) : FloatingActionButton(ctx, attributeSet) {

    var onClick = {}

    var icon: Int? = R.drawable.sign_up_button
        set(value) {
            when (value) {
                field -> Unit
                null -> {
                    toHide { field = value }
                }
                else -> {
                    toHide {
                        setImageResource(value)
                        setColorFilter(colorFilter) //TODO
                        fromHide {
                            field = value
                        }
                    }
                }
            }
        }

    override fun onFinishInflate() {
        super.onFinishInflate()
        setOnClickListener {
            rotate(-20f, {
                rotate(40f, {
                    rotate(-20f, {
                        onClick()
                    })
                })
            })
        }
    }

    private val inter = AccelerateInterpolator(0f)
    private val inter2 = DecelerateInterpolator(0f)
    private val inter3 = AccelerateDecelerateInterpolator()

    private fun toHide(after: () -> Unit) {
        animate().setInterpolator(inter).setDuration(200).translationY(resources.toPx(0)
                .toFloat()).doAfter(after)
    }

    private fun fromHide(after: () -> Unit) {
        animate().setInterpolator(inter2).setDuration(200).translationY(0f).doAfter(after)
    }

    private fun rotate(how: Float, after: () -> Unit) {
        animate().rotationBy(how).setInterpolator(inter3).setDuration(100).doAfter(after)
    }

}

class AFabActor(
        private val fabView: AFloaterView,
        private val s: Tunnel,
        private val enabledStateActor: EnabledStateActor,
        private val contentActor: ContentActor,
        private val label: TextView
) : IEnabledStateActorListener {

    private val ctx by lazy { fabView.context }
    private val colorsActive by lazy { getColorStateList(ctx, R.color.text) }
    private val colorsAccent by lazy { getColorStateList(ctx, R.color.playMode) }
    init {
        contentActor.onDashOpen += { dash -> when {
            dash == null -> reset()
            dash.menuDashes.first != null -> {
                val d = dash.menuDashes.first!!
                enabledStateActor.listeners.remove(this)
                val icon = d.icon as Int
                fabView.icon = icon
                fabView.onClick = {
                    d.onClick?.invoke(d)
                }
            }
            else -> fabView.icon = null
        }}

        reset()
    }

    private fun reset() {
        enabledStateActor.listeners.add(this)
        enabledStateActor.update(s)
       // label.visibility = View.VISIBLE
       // fabView.icon = R.drawable.sign_up_button
        fabView.onClick = {
            s.enabled %= !s.enabled()
        }
    }

    override fun startActivating() {
        fabView.icon=R.drawable.sign_up_button_off
        label.visibility = View.VISIBLE
        fabView.isEnabled = false
     //   fabView.setColorFilter(ctx.resources.getColor(R.color.colorBackground))
        Log.d("srart","A")
    }

    override fun startDeactivating() {
        fabView.icon=R.drawable.sign_up_button_off
        label.visibility = View.VISIBLE
        fabView.isEnabled = false
        Log.d("srart","D")
     //   fabView.setColorFilter(ctx.resources.getColor(R.color.colorBackground))
    }

    override fun finishActivating() {

        fabView.icon=R.drawable.sign_up_button
        label.visibility = View.INVISIBLE
        fabView.isEnabled = true
        Log.d("finish","A")
    //    fabView.setColorFilter(ctx.resources.getColor(R.color.colorBackground))
    }

    override fun finishDeactivating() {
        label.visibility = View.VISIBLE
        fabView.icon=R.drawable.sign_up_button_off
        fabView.isEnabled = true
        Log.d("finish","D")
        //     fabView.setColorFilter(ctx.resources.getColor(R.color.colorBackground))
    }

}
