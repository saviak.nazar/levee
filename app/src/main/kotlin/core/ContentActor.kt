package core

import adblocker.DASH_ID_HOSTS_COUNT
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.util.Log
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.FrameLayout
import android.widget.ImageView
import filter.DASH_ID_WHITELIST
import io.codetail.animation.ViewAnimationUtils
import io.codetail.widget.RevealFrameLayout

private data class OpenDash(val d: Dash, val x: Int, val y: Int)

class ContentActor(
        private val ui: UiState,
        private val reveal: RevealFrameLayout,
        private val revealContainer: FrameLayout,
        private val topBar: ATopBarView,
        private val radiusSize: Float,
        private val slide: SlidingLayout,
        private val logo : ImageView
) {

    val onDashOpen = mutableListOf<(Dash?) -> Unit>()

    private val dashesCaches: MutableMap<String, Any?> = mutableMapOf()
    private var openDash: OpenDash? = null

    fun reveal(dash: Dash, x: Int, y: Int, after: () -> Unit = {}) {
        if (openDash?.d == dash) return
        openDash = OpenDash(dash, x, y)
        Log.d("ContentActor", "1if")
        System.out.println("iiiiiii")
        System.out.println(openDash?.x)
       // toggleMenu(topBar,slide)

        val view = dashesCaches.get(dash.id)
        if (view is View) {
            revealContainer.addView(view)

        } else if (dash.isSwitch) {
            dash.checked = !dash.checked
            Log.d("ContentActor","addView2")
            after()
            return
        } else if (!dash.hasView) {
            ui.infoQueue %= ui.infoQueue() + Info(InfoType.CUSTOM, dash.description)
            Log.d("ContentActor","addView3")

           // logo.visibility=View.VISIBLE
            after()
            return
        }

        topBar.bg = dash.topBarColor
        topBar.mode = ATopBarView.Mode.BAR
        reveal.visibility = android.view.View.VISIBLE

        val animator = ViewAnimationUtils.createCircularReveal(
                revealContainer, 0, y, 0f, 0f)
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.duration = 1
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                var v = view
                if (v == null) {
                    v = dash.createView(reveal)
                    if (v is View) {
                        dashesCaches.put(dash.id, v)
                        revealContainer.addView(v)
                    }
                }

                onDashOpen.forEach { it(dash) }
                dash.onDashOpen()
                after()
            }
        })
        animator.start()
    }
    fun toggleMenu(v: View,
                   slidingLayout:SlidingLayout) {
        slidingLayout.toggleMenu()
    }

    fun retutnSlider():SlidingLayout{
        return slide
    }

    fun back(after: () -> Unit = {}): Boolean {
        topBar.bg = null
        topBar.mode = ATopBarView.Mode.BAR

        if (openDash == null) {
            after()
            return false
        }

        val (dash, x, y) = openDash!!
        val animator = ViewAnimationUtils.createCircularReveal(
                revealContainer, getWidth(x), y, radiusSize, 0f)
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.duration = 300
       animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                openDash = null
                revealContainer.removeAllViews()
                reveal.visibility = View.GONE
                after()
            }
        })
        animator.start()
        dash.onBack()
        onDashOpen.forEach { it(null) }
        return true
    }

    private fun getWidth(x: Int): Int {
        return if (x == X_END) topBar.measuredWidth
        else x
    }

    companion object {
        val X_END = -1
    }
}
