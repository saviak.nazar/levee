package core

import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CoordinatorLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.LazyKodeinAware
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import gs.environment.ComponentProvider
import gs.environment.Journal
import gs.environment.Worker
import gs.environment.inject
import okhttp3.*
import gs.obsolete.Sync
import com.google.gson.GsonBuilder
import gs.presentation.isWrongInstance
import gs.property.Device
import gs.property.IWhen
import gs.property.Version
import io.codetail.widget.RevealFrameLayout
import nl.komponents.kovenant.task
import org.blokada.BuildConfig
import org.blokada.R
import tunnel.startAskTunnelPermissions
import tunnel.stopAskTunnelPermissions
import java.lang.ref.WeakReference
import android.support.v7.app.AlertDialog
import android.support.v7.widget.SwitchCompat
import android.util.Log
import android.widget.*
import filter.DASH_ID_WHITELIST
import okhttp3.*
import java.io.IOException
import java.util.*
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager


const val ID_FILTER = "test.igor.red"
const val URL_FILTER = "https://test.igor.red/adsafe/hosts.txt"
class MainActivity : AppCompatActivity(), LazyKodeinAware {

    override val kodein = LazyKodein(inject)

    var landscape = false
    private val timing:Long?=null
    private val password:String?= null

    private var infoView: AInfoView? = null
    private var grid: AGridView? = null
    private var grid_togle: AGridView? = null
    private var topBar: ATopBarView? = null
    private var sliderMenu: SlidingLayout? = null
    private var contentActor: ContentActor? = null

    private val enabledStateActor: EnabledStateActor by instance()
    private val activityContext: ComponentProvider<Activity> by instance()
    private val activityProvider: ComponentProvider<MainActivity> by instance()
    private val j: Journal by instance()

    private val s: Filters by instance()
    private val t: Tunnel by instance()
    private val d: Device by instance()
    private val ui: UiState by instance()
    private val pages: Pages by instance()
    private var activeDataModel: ActivateData? = null
    private var sPref:SharedPreferences?=null
    final val SAVED_TEXT:String = "value"
    final val SAVED_VERSION:String = "version"
    var etText:TextView? = null
    var days:TextView?= null
    var value:Long=0

    private val ictx: Worker by kodein.with("infotext").instance()
    private var currentlyDisplayed: InfoType? = null
    private val logo by lazy { findViewById(R.id.topbar_logo_menu) as ImageView }
    private val but by lazy{findViewById(R.id.activate) as Button}
    private val logoBar by lazy { findViewById(R.id.topbar_logo) as ImageView }
    private val tapActive by lazy { findViewById(R.id.activation_stats) as TextView }
    // TODO: less stuff in this class, more modules

    private var listener11: IWhen? = null
    private var listener12: IWhen? = null
    private var listener13: gs.property.IWhen? = null

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        activityContext.set(this)
        activityProvider.set(this)
        if (isWrongInstance(this)) {
            finish()
            return
        }

        landscape = resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE

        setContentView(R.layout.activity_main)

        infoView = findViewById(R.id.info) as AInfoView
        enabledStateActor.listeners.add(enabledStateListener)

        topBar = findViewById(R.id.topbar) as ATopBarView
        sliderMenu = findViewById(R.id.sliding_layout) as SlidingLayout

        val getRadiusSize = {
          /*  val size = android.graphics.Point()
            windowManager.defaultDisplay.getSize(size)
            if (landscape) size.x.toFloat()
            else size.y.toFloat()*/
        }

        contentActor = ContentActor(
                ui = ui,
                reveal = findViewById(R.id.reveal) as RevealFrameLayout,
                revealContainer = findViewById(R.id.reveal_container) as FrameLayout,
                topBar = topBar!!,
                radiusSize = 0f,
                slide = sliderMenu!!,
                logo = logoBar
        )


        ATopBarActor(
                logo = logoBar,
                slide = sliderMenu!!,
                xx = kodein,
                m = t,
                v = topBar!!,
                enabledStateActor = enabledStateActor,
                contentActor = contentActor!!,
                infoView = infoView!!,
                window = window,
                ui = ui,
                pages = pages
        )

        grid = findViewById(R.id.grid) as AGridView
        grid?.setPadding(48, 0, 0, 0)
        grid?.ui = ui
        grid?.contentActor = contentActor

        grid_togle = findViewById(R.id.grid_togle) as AGridView
        grid_togle?.setPadding(0, 0, 0, 0)
        grid_togle?.ui = ui
        grid_togle?.contentActor = contentActor



        val updateItems = {
                ui.dashes().subList(0,5)
        }

        val updateItemsToggle = {
            ui.dashes().filter(Dash::isSwitch)
        }

        logo?.setOnClickListener(View.OnClickListener { v ->
            toggleMenu(v,sliderMenu!!)
            logoBar.visibility=View.VISIBLE
            Log.d("MainActivity", "FindMainActivity")
        })

        grid?.items = updateItems()
        listener11 = ui.dashes.doOnUiWhenSet().then {
            grid?.items = updateItems()
        }


        grid?.onScrollToTop = { isTop ->
            if (!isTop && !landscape) topBar?.mode = ATopBarView.Mode.BAR
        }

        grid_togle?.items = updateItemsToggle()
        listener11 = ui.dashes.doOnUiWhenSet().then {
            grid_togle?.items = updateItemsToggle()
        }




        val fab = findViewById(R.id.fab) as AFloaterView



       // etText=findViewById(R.id.status)
        value = System.currentTimeMillis()+691200000
        days = findViewById(R.id.daysLeft)
        if(loadText().equals("")){
            saveData(value)
          //  Toast.makeText(this, "data is null", Toast.LENGTH_SHORT).show()
            etText?.text=(minus(loadText()!!.toLong(),System.currentTimeMillis())).toString()
            val sb = StringBuilder()
            sb.append(getString(R.string.note)).append(" 7 ").append(getString(R.string.days))
            days?.text = sb.toString()
            but?.visibility = View.VISIBLE
            AFabActor(fab, t, enabledStateActor, contentActor!!,tapActive)
            saveVersion("trial")
            buttonAction(but!!)
        }
        else if(minus(loadText()!!.toLong(),System.currentTimeMillis())<0){

           // Toast.makeText(this, "Code expired", Toast.LENGTH_SHORT).show()
           // Toast.makeText(this, "${System.currentTimeMillis()}", Toast.LENGTH_SHORT).show()
            but?.visibility = View.VISIBLE
            val sb = StringBuilder()
            sb.append(getString(R.string.note)).append(daysLeft(minus(loadText()!!.toLong(),System.currentTimeMillis())).toString()).append(getString(R.string.days))
            days?.text = sb.toString()
            t.enabled %= !t.enabled()
            fab.icon = R.drawable.sign_up_button_off
            buttonAction(but!!)
        }
        else if(loadVersion().equals("trial")){

            // Toast.makeText(this, "Code expired", Toast.LENGTH_SHORT).show()
           // Toast.makeText(this, "${System.currentTimeMillis()}", Toast.LENGTH_SHORT).show()
            but?.visibility = View.VISIBLE
            val sb = StringBuilder()
            AFabActor(fab, t, enabledStateActor, contentActor!!,tapActive)
            sb.append(getString(R.string.note)).append(daysLeft(minus(loadText()!!.toLong(),System.currentTimeMillis())).toString()).append(getString(R.string.days))
            days?.text = sb.toString()
            buttonAction(but!!)
        }
        else{
            etText?.text=(minus(loadText()!!.toLong(),System.currentTimeMillis())).toString()
          //  Toast.makeText(this, "Code is good", Toast.LENGTH_SHORT).show()
            but?.visibility = View.INVISIBLE
            val sb = StringBuilder()
            sb.append(getString(R.string.note)).append(daysLeft(minus(loadText()!!.toLong(),System.currentTimeMillis())).toString()).append(getString(R.string.days))
            days?.text = sb.toString()
            AFabActor(fab, t, enabledStateActor, contentActor!!,tapActive)
            //buttonAction(but!!)
        }








       // val fab = findViewById(R.id.fab) as AFloaterView

        if (landscape) {
            topBar?.mode = ATopBarView.Mode.BAR
            grid?.setPadding(0, 0, 0, 0)
            grid?.landscape = true
            grid?.adapter = grid?.adapter // To refresh grid
            val lp = fab.layoutParams as CoordinatorLayout.LayoutParams
            lp.gravity = android.view.Gravity.BOTTOM or android.view.Gravity.RIGHT
            fab.layoutParams = lp
            fab.visibility=View.INVISIBLE
        }


        staticContext.set(java.lang.ref.WeakReference(this))

//        val d = AWelcomeDialog(this, contentActor!!)
//        listener13 = ui.seenWelcome.doOnUiWhenSet().then {
//            if (d.shouldShow()) d.show()
//    /    }

        val welcome: Welcome by instance()
        val version: Version by instance()
        //val m = WelcomeDialogManager(kodein, BuildConfig.VERSION_CODE, {})
        //m.run()
    }
    fun buttonAction(button: Button){
        button.setOnClickListener() {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm!!.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            val editView = layoutInflater.inflate(R.layout.edit_text_layout, null)
            val password = editView.findViewById(R.id.passwordInputField) as EditText
            val builder = AlertDialog.Builder(this@MainActivity)


            // val option = layoutInflater.inflate(R.layout.view_topbar,null)
           // builder.setTitle("Enter licence code")
            builder.setView(editView)
            builder.setPositiveButton(R.string.active) { dialog, which ->
                // Do something when user press the positive button
                // Toast.makeText(applicationContext,"Дякуємо за співпрацю",Toast.LENGTH_SHORT).show()

                if (password.text.toString()!="") {
                 //   Toast.makeText(applicationContext, "you enter correct code", Toast.LENGTH_SHORT).show()
                    checkKeyCodeFromBackEnd(password.text.toString())
                } else {
                    Toast.makeText(applicationContext, R.string.badPass, Toast.LENGTH_SHORT).show()

                }
                imm.hideSoftInputFromWindow(editView.windowToken,0);

            }


            ////end activation ***


            // Display a neutral button on alert dialog
            builder.setNeutralButton(R.string.cancel) { _, _ ->
               // Toast.makeText(applicationContext, "Please enter code if you want Levee work correctly", Toast.LENGTH_SHORT).show()
                imm.hideSoftInputFromWindow(editView.windowToken,0);
            }

            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()

            // Display the alert dialog on app interface
            dialog.show()
        }
    }



    fun saveData(value:Long) {
        sPref = getPreferences(Context.MODE_PRIVATE)
        val ed = sPref?.edit()
        ed?.putString(SAVED_TEXT, value.toString())
        ed?.commit()
        // Toast.makeText(this, "Text saved", Toast.LENGTH_SHORT).show()
    }

    fun loadText():String? {
        sPref = getPreferences(Context.MODE_PRIVATE)
        val savedText = sPref?.getString(SAVED_TEXT, "")
        //  Toast.makeText(this, "Text loaded", Toast.LENGTH_SHORT).show()
        return savedText
    }

    fun saveVersion(value:String){
        sPref = getPreferences(Context.MODE_PRIVATE)
        val ed = sPref?.edit()
        ed?.putString(SAVED_VERSION, value.toString())
        ed?.commit()
    }
    fun loadVersion():String? {
        sPref = getPreferences(Context.MODE_PRIVATE)
        val savedText = sPref?.getString(SAVED_VERSION, "")
        //  Toast.makeText(this, "Text loaded", Toast.LENGTH_SHORT).show()
        return savedText
    }

    fun minus(value1:Long, value2:Long):Long{
        println("mainActivityMinus, ${value1-value2}")
        return value1-value2
    }
    fun daysLeft(value:Long):String{
        val dayX = (value / (1000 * 60 * 60 * 24))

        return dayX.toString()
    }
    fun returnTopBar():ATopBarView{
        return topBar!!
    }
    fun returnSlide():SlidingLayout{
        sliderMenu = findViewById(R.id.sliding_layout) as SlidingLayout
        return sliderMenu!!
    }



    val activityResultListeners = mutableListOf({result: Int, data: Intent? -> })

    fun addOnNextActivityResultListener(listener: (result: Int, data: Intent?) -> Unit) {
        activityResultListeners.add(listener)
      //  fab.visibility = View.INVISIBLE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        activityResultListeners.forEach { it(resultCode, data) }
        activityResultListeners.clear()
        stopAskTunnelPermissions(resultCode)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        val fab = findViewById(R.id.fab) as AFloaterView
        fab.visibility = View.VISIBLE

        if (intent.getBooleanExtra("notification", false)) {
            val n: NotificationManager = inject().instance()
            n.cancel(1)
        }
        if (intent.getBooleanExtra("askPermissions", false)) {
            j.log("Started main activity for askForPermissions permissions")
            task {
                try {
                    askPermissions()
                } catch (e: Exception) {
                    t.active %= false
                }
//                engineState.enabled.changed()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        infoListener = ui.infoQueue.doWhenChanged().then { infoQueueHandler(ui.infoQueue()) }
    }

    override fun onStop() {
        ui.infoQueue.cancel(infoListener)
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        enabledStateActor.update(t)
        infoQueueHandler(ui.infoQueue())
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
//        staticContext.set(WeakReference(null as Activity?))
        super.onDestroy()
        activityContext.unset()
        activityProvider.unset()
        enabledStateActor.listeners.remove(enabledStateListener)
    }

    override fun onBackPressed() {
        if (!(contentActor?.back() ?: false)) super.onBackPressed()
    }

    private val display = { i: Info ->
        currentlyDisplayed = if (i.type == InfoType.CUSTOM) null else i.type
        task(ictx) {
            infoView?.message = handleInfo(i)
        }
    }

    var infoListener: IWhen? = null
    val infoQueueHandler = { queue: List<Info> ->
        val queue = ui.infoQueue()
        when (queue.size) {
            0 -> Unit
            1 -> {
                if (queue.first().type != currentlyDisplayed) display(queue.first())
                ui.infoQueue %= emptyList()
            }
            in 2..4 -> {
                queue.forEach { display(it) }
                ui.infoQueue %= emptyList()
            }
            else -> {
                display(queue.last())
                ui.infoQueue %= emptyList()
            }
        }
    }

    fun toggleMenu(v: View,
                   slidingLayout:SlidingLayout) {
        slidingLayout.toggleMenu()
    }

    private fun handleInfo(i: Info): String {
        return when (i.type) {
            InfoType.CUSTOM -> when {
                i.param is String -> i.param
                i.param is Int -> brandedString(i.param)
                else -> throw Exception("custom info without a param")
            }
            InfoType.ERROR -> brandedString(R.string.main_paused)
            InfoType.PAUSED -> brandedString(R.string.main_paused)
            InfoType.PAUSED_TETHERING -> brandedString(R.string.main_paused_autoenable_tethering)
            InfoType.PAUSED_OFFLINE -> brandedString(R.string.main_paused_autoenable)
            InfoType.ACTIVATING -> brandedString(R.string.main_loading)
            InfoType.ACTIVE -> brandedString(R.string.main_active)
            InfoType.DEACTIVATING -> brandedString(R.string.main_deactivating_new)
            InfoType.NOTIFICATIONS_DISABLED -> brandedString(R.string.notification_disabled)
            InfoType.NOTIFICATIONS_ENABLED -> brandedString(R.string.notification_enabled)
            InfoType.NOTIFICATIONS_KEEPALIVE_ENABLED -> brandedString(R.string.notification_keepalive_enabled)
            InfoType.NOTIFICATIONS_KEEPALIVE_DISABLED -> brandedString(R.string.notification_keepalive_disabled)
        }
    }

    private val enabledStateListener = object : IEnabledStateActorListener {

        override fun startDeactivating() {
            ui.infoQueue %= listOf(Info(InfoType.DEACTIVATING))
        }

        override fun finishDeactivating() {
            when {
                !t.enabled() -> ui.infoQueue %= listOf(Info(InfoType.PAUSED))
                t.restart() && d.tethering() ->
                    ui.infoQueue %= listOf(Info(InfoType.PAUSED_TETHERING))
                t.restart() && !d.connected() -> ui.infoQueue %= listOf(Info(InfoType.PAUSED_OFFLINE))
                else -> ui.infoQueue %= listOf(Info(InfoType.ERROR))
            }
        }

        override fun startActivating() {
            ui.infoQueue %= listOf(Info(InfoType.ACTIVATING))
        }

        override fun finishActivating() {
            ui.infoQueue %= listOf(Info(InfoType.ACTIVE))
        }

    }

    private fun brandedString(resId: Int): String {
        return getBrandedString(resId)
    }

    companion object {
        var staticContext = Sync(WeakReference(null as Activity?))
        fun askPermissions() {
            val act = staticContext.get().get()
            if (act == null) {
//                Log.e("blokada", "Trying to start main activity")
//                val ctx: Context = globalKodein().instance()
//                val intent = Intent(ctx, MainActivity::class.java)
//                intent.putExtra("askPermissions", true)
//                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//                ctx.startActivity(intent)
                throw Exception("starting MainActivity")
            }

            val task = startAskTunnelPermissions(act).promise.get()
            if (!task) { throw Exception("Could not get tunnel permissions") }
        }

//        fun share(id: String) {
//            val act = staticContext.get().get() ?: throw Status.exception("activity not started")
//            val intent = Intent(Intent.ACTION_SEND)
//            intent.type = "text/plain"
//            intent.putExtra(Intent.EXTRA_SUBJECT,
//                    act.getRandomString(R.array.viral_subject, R.string.branding_app_name))
//            intent.putExtra(Intent.EXTRA_TEXT,
//                    "http://play.google.com/store/apps/details?id=${act.packageName}&referrer=${id}")
//            act.startActivity(intent)
//        }
    }

    private fun checkKeyCodeFromBackEnd(key: String){
        val request = Request.Builder()
                .url("https://test.igor.red/adsafe/reg?code=$key")
                .build()
        val client = OkHttpClient()
        var activator:Long = 0
        client.newCall(request).enqueue(
                object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
//                Toast.makeText(this@MainActivity, "Error request!", Toast.LENGTH_LONG).show()
                    }

                    override fun onResponse(call: Call, response: Response) {
                        val body = response.body()?.string()
                        println("mainActivity:$body")
//                var jsonArray = JSONArray(response)
//                var success: Int = jsonArray.getJSONObject()
                        val gson = GsonBuilder().create()
                        val activateRequestModel = gson.fromJson(body, ActivateRequest::class.java)
                        println("mainActivity, '/n")
                        println("mainActivity, text:${activateRequestModel.text}")
                        println("mainActivity, success:${activateRequestModel.success}")
                        println("mainActivity, ts:${activateRequestModel.ts}")
                        runOnUiThread {
                            Toast.makeText(this@MainActivity, activateRequestModel.text, Toast.LENGTH_LONG).show()
                            if (activateRequestModel.success==1) {

                                activator=activateRequestModel.ts*1000
                                saveData(activator)
                                val sb = StringBuilder()
                                sb.append(getString(R.string.note)).append(daysLeft(minus(loadText()!!.toLong(),System.currentTimeMillis())).toString()).append(getString(R.string.days))
                                days?.text = sb.toString()
                                but.visibility=View.INVISIBLE
                                saveVersion("nottrial")
                                println("mainActivityActivator, $activator")
                                println("mainActivityCurrent, ts:${System.currentTimeMillis()}")


                            }
                            else{
                               // activator=System.currentTimeMillis()
                                println("mainActivity, $activator")
                                but.visibility=View.VISIBLE
                            }
                        }
                    }
                })
        println("mainActivityActivator, $activator")
        println("mainActivityCurrent, ts:${System.currentTimeMillis()}")
    }
}

fun Context.getBrandedString(resId: Int): String {
    return getString(resId, getString(R.string.branding_app_name_short))
}

class ActivateRequest(var success: Int, var text: String, var ts: Long)

class ActivateData(var endDate: Long, var type: Int)